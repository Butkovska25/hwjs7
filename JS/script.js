/* 
1. Метод forEach() використовується для перебору масиву та виклику 
для кожного елемента функції callback. Він нічого не повертає.
2. A = []; або A.length = 0
3. Array.isArray()
*/

let arr = ['hello', 'world', 23, '23', null, 'bye', 44];
function filterBy(arr, type) {return arr.filter(item=> typeof item == type) }
console.log(filterBy(arr, 'number'));
